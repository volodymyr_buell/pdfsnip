msgid ""
msgstr ""
"Project-Id-Version: PDF-Shuffler 0.4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-12-14 18:57+0100\n"
"PO-Revision-Date: 2009-06-25 14:11+0100\n"
"Last-Translator: netcelli <netcelli@dchublist.com>\n"
"Language-Team: netcelli <netcelli@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Italian\n"
"X-Poedit-Country: ITALY\n"

#: pdfshuffler:101 pdfshuffler:520
#, python-format
msgid "File %s does not exist"
msgstr "Il file %s non esiste"

#: pdfshuffler:205 pdfshuffler:243
msgid "Delete Page(s)"
msgstr "Cancella pagina(e)"

#: pdfshuffler:212
msgid "Import pdf"
msgstr "Importa PDF"

#: pdfshuffler:219
msgid "Export pdf"
msgstr "Esporta PDF"

#: pdfshuffler:226
msgid "About"
msgstr ""

#: pdfshuffler:240
msgid "Rotate Page(s) Clockwise"
msgstr "Ruota pagina(e) in senso orario"

#: pdfshuffler:241
msgid "Rotate Page(s) Counterclockwise"
msgstr "Ruota pagina(e) in senso antiorario"

#: pdfshuffler:242
msgid "Crop Page(s)"
msgstr "Taglia pagina(e)"

#: pdfshuffler:358
msgid "page"
msgstr "pagina"

#: pdfshuffler:384
msgid "Export ..."
msgstr "Esporta ..."

#: pdfshuffler:393 pdfshuffler:501
msgid "PDF files"
msgstr "File PDF"

#: pdfshuffler:398 pdfshuffler:496
msgid "All files"
msgstr "Tutti i file"

#: pdfshuffler:416
#, python-format
msgid "Error writing file: %s"
msgstr ""

#: pdfshuffler:435
#, python-format
msgid "File %s is encrypted."
msgstr "Il file %s è criptato."

#: pdfshuffler:436
msgid "Support for such files has not been implemented yet."
msgstr "Il supporto per questo tipo di file non è ancora stato implementato"

#: pdfshuffler:437
msgid "File export failed."
msgstr "Esportazione dei file fallita"

#: pdfshuffler:479
msgid "exporting to:"
msgstr "esporta in:"

#: pdfshuffler:486
#, fuzzy
msgid "Import..."
msgstr "Importa ..."

#: pdfshuffler:514
msgid "OpenDocument not supported yet!"
msgstr ""

#: pdfshuffler:516
msgid "Image file not supported yet!"
msgstr ""

#: pdfshuffler:518
msgid "File type not supported!"
msgstr ""

#: pdfshuffler:522
msgid "Closed, no files selected"
msgstr "Chiuso, nessun file selezionato"

#: pdfshuffler:821
msgid "Crop Selected Page(s)"
msgstr "Taglia pagina(e) seleziona(e)"

#: pdfshuffler:829
msgid "Crop Margins"
msgstr "Taglia margini"

#: pdfshuffler:836
#, python-format
msgid "% of width"
msgstr "% in larghezza"

#: pdfshuffler:836
#, python-format
msgid "% of height"
msgstr "% in altezza"

#: pdfshuffler:837
msgid "Left"
msgstr "Sinistra"

#: pdfshuffler:837
msgid "Right"
msgstr "Destra"

#: pdfshuffler:837
msgid "Top"
msgstr "Alto"

#: pdfshuffler:837
msgid "Bottom"
msgstr "Basso"

#: pdfshuffler:872
msgid "Dialog closed"
msgstr "FInestra di dialogo chiusa"

#: pdfshuffler:887
msgid ""
"PDF-Shuffler is a simple pyGTK utility which lets you merge, split and "
"rearrange PDF documents. You can also rotate and crop individual pages of a "
"pdf document."
msgstr ""
